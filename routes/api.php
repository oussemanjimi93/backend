<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/person-list', 'App\Http\Controllers\UserController@personList');
Route::put('/person/{id}', 'App\Http\Controllers\UserController@updatePerson');
Route::delete('/person/{id}', 'App\Http\Controllers\UserController@deletePerson');
Route::post('/person', 'App\Http\Controllers\UserController@addPerson');




