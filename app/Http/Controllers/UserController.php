<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Person;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function personList(Request $request)
    {
        $person = Person::All();
        return response()->json($person, 200);
    }

    public function updatePerson(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'string|max:50',
            'last_name' => 'string|max:50',
            'email' => 'email',
            'adresse' => 'string|max:50'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => 'error lors du mise à jour'
            ], 400);
        }
        $person = Person::where('id', $id)->first();
        if($person == null)
        {
            return response()->json([
                'error' => "error lors du mise à jour"], 500);
        }
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $email = $request->email;
        $adresse = $request->adresse;

        
        try {
            if($request->filled('first_name'))
            {
                $person->first_name = $first_name;
            }else{
                $person->first_name = $first_name;
            }
            if($request->filled('last_name'))
            {
                $person->last_name = $last_name;
            }
            if($request->filled('email'))
            {
                $person->email = $email;
            }
            if($request->filled('adresse'))
            {
                $person->adresse = $adresse;
            }
            $person->save();
            return response()->json(['message'=>"user a jour"],200);

        } catch (\Throwable $th) {
            return response()->json(['error'=>$th->getMessage()],500);
            //throw $th;
        }
    }

    public function deletePerson($id){
        $person = Person::find($id);
        try {
            $person->delete();
            return response()->json(['message'=>"User deleted"],200);

        } catch (\Throwable $th) {
            return response()->json(['error'=>$th->getMessage()],500);

            //throw $th;
        }

    }
    public function addPerson(Request $request){
        try {
            Person::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'adresse' => $request->adresse,
                'email' => $request->email
            ]);
            return response()->json(['message'=>"User created"],200);

        } catch (\Throwable $th) {
            return response()->json(['error'=>$th->getMessage()],500);

            //throw $th;
        }

    }

}
